##### react原理-setState更新是异步的(9分钟)

1. 介绍setState其实是异步的，与vue是一致
2. 定义一个方法，在方法里面 setState 之后，打印count数据，发现是旧数据，说明它是异步的
3. 直接setState 3 次，介绍只会执行最后一次，在render中打印，发现只打印出一次，说明它是合并执行的
4. 对着文档总结



##### react原理-setState的第二种写法(8分钟)

1. 介绍刚才的count，多次执行最终会被合并，如果真的有这么一个场景，需要我依赖上一次执行完之后的值，再做修改，那么可以使用这种写法

2. 介绍语法 setState((preState) => {})， 打印 preState，告诉同学们是上一次setState的结果

3. 连续执行3遍，发现数据改变了，

   但是也只render了一遍，告诉学生，类似写了三个 setTimeout,依次执行

##### react原理-setState的第二个参数(6分钟)

1. 介绍 setState 有第二个参数，是一个回调函数，保证在 页面更新(render完之后会执行)，但是现在一般不怎么使用了
2. 虽然能保证在上一次修改完数据后才渲染，但是有回调地狱和多次render

##### react原理-setState小结(5分钟)

 ```
   /*
     学习目标：setStata小结
     1. ✅setState(更新的对象)
     场景：不需要连续调用时，正常使用即可
     特点：连续调用，React会做覆盖处理，产生合并效果
   
     2. ✅setState((旧的state) => 新的状态 )
     场景：需要根据前面的值，计算新的值
     优点：React会做等待上次计算处理，减少render次数，产生合并效果
   
     3. 👎setState(更新的对象， () => {})
     场景： 需要根据前面的值，计算新的值
     注意：工作中几乎不用，这是早期的写法
     缺点：
       3.1 不会产生合并效果，频繁操作dom
       3.2 会产生回调地狱，增加理解成本
   */
 ```





##### 组件更新机制(5分钟)

1. 介绍父组件重新渲染时，也会重新渲染子组件
2. 代码演示，复制静态结构，强调两个子组件，都没有接收过props，页面也没有更新过。但是 App更新了，可以看到子组件的componentDidUpdate 依然打印了内容，说明默认会导致所有后代组件更新
3. 再演示兄弟组件，对着Son组件，添加状态和修改，发现不会影响兄弟组件
4. 一直渲染，的确会很浪费性能，我们先使用，等会再来完成优化的事

##### 组件更新优化 - shouldComponentUpdate(8分钟)

1. 介绍功能第一，优化第二，实现功能，再去优化
2. 对着文档，先介绍 减轻state，跟渲染无关的数据，不要放到state中，直接放到this上，例如定时器
3. 引入 shouldComponentUpdate，打开react生命周期官网，介绍这个不常用的生命周期，也提醒学生项目较少使用
4. 演示返回 ture 会渲染，返回 false 会阻断渲染，避免不必要的更新

##### 组件更新优化-加入判断(8分钟)

1. 介绍直接返回 true/false，会使到某些业务无法实现，案例上再添加一个属性，要实现一个count更新，msg不更新
2. 打开官网，介绍 shouldComponentUpdate 可以接受参数，nextProps就是将要接收的数据，可以跟this.props做判断，如果不同，说明需要重新渲染

组件更新优化-React.PureComponent纯组件(5分钟)

1. 介绍 shouldComponentUpdate 需要一个一个属性去比较，当属性较多时，代码乱也不好维护
2. PureComponent 内部自动实现了 shouldComponentUpdate 钩子，不需要手动比较
3. 介绍不要滥用纯组件，比如返回是写死的组件，直接return false即可

##### 深浅比较介绍(8分钟)

##### 案例介绍(6分钟)

1. 向学生说明说了深浅比较，都是为了扩展视野，实际代码就1行

2.   this.state.list.push(4)

     this.setState({ list: this.state.list })



##### react-单页面应用介绍(5分钟)

1. 对着文档介绍即可



##### react路由-底层原理介绍(10分钟)

1. 对着文档介绍路由原理，本质其实跟vue一样
2. 介绍使用原生的方法，来模拟它的实现
3. 吧静态html结构复制，对着步骤123完成，注意最后是 绑定的方法名是 hashchange

##### react路由-路由使用(10分钟)

1. 回顾vue中路由的实现
2. 介绍react使用的版本(不要用最新，用项目使用最多的即可)
3. 对着文档介绍，然后代码实现
   1. HashRouter 类似 vue中的 new VueRouter()，创建路由实例，注意需要包裹住所有代码
   2. Route 就是 路由实例中的 routes，编写路由页面
   3. Link 就是 routerLink，负责路由的跳转
   4. 额外介绍下Route的原理，当路由匹配时就返回组件，否则返回null,所以没有内容渲染

##### react路由-Router组件的使用说明(6分钟)

​	文档介绍即可，有以下步骤

1. 告诉学生除了hash模式，还有history模式，本质上是一样，history 通过监听 'popstate' 实现
2. 如果想改模式，只需要调用 BrowserRouter 组件即可
3. 但是每次都该组件比较麻烦，建议可以解构时，BrowserRouter  as Router，以后只需要改组件名即可

##### react路由-NavLink和Link的区别(10分钟)

​	文档介绍即可，有以下步骤

1. 介绍相同点都是渲染a标签，但是NavLink可以实现高亮的效果，默认添加类名active，演示添加样式，看到效果
2. active同名概率大，使用activeClassName='test'指定另外的类名
3. 介绍exact，在精准匹配时候才生效
4. 可以介绍下为什么不直接使用a标签，因为路由组件会根据模式，自动判断添加#

##### react路由-Route组件的使用说明(10分钟)

​	文档介绍即可，所有功能演示一遍



##### react路由-Switch组件和404通配(5分钟)

1. 介绍一下 js 中 switch 的作用，当匹配到一个条件时就执行 break，停止执行其他

2. 把刚才的重复渲染的路由页面都复制进 Switch 组件，发现只渲染一个，

   说明在`Switch`组件中，匹配到第一个匹配的组件，停止向下匹配

3. 再定义一个 NotFount 组件，通过不写path通配，放在Switch 组件的最后，实现统配

   可以移到前面，发现路径怎么样变化，都是NotFount，其实跟vue的统配一样

##### react路由-嵌套路由(10分钟)

1. 介绍react也可以实现嵌套路由，也就是vue中的多级路由页面

2. 新建组件Home1/Home2/Home3,都放到Home组件的return里面，浏览器可以看到3个组件都出来了

3. 在Home里面，也使用Switch -和 Route，就是嵌套路由，为了看到效果加上Link

   <Switch><Route path='/home/home1' component='Home1'>

   <Link to='/home/home1'>

4. 修改/home/home1为/home，可以看到嵌套路由出现

5. 提醒学生，不要设置 exact，会导致父子组件都无法渲染

   

   